// Soal 2
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

function jumlah_kata(str) { 
    return str.split(/\S+/).length - 1;
};

console.log(jumlah_kata(kalimat_1));
console.log(jumlah_kata(kalimat_2));
console.log(jumlah_kata(kalimat_3));

// Soal 1
var tanggal = 13;
var bulan = 3;
var tahun = 2022;

switch (bulan) {
    case 1: {
        bulan = "Januari"
        break;
    }
    case 2: {
        bulan = "Februari"
        break;
    }
    case 3: {
        bulan = "Maret"
        break;
    }
    case 4: {
        bulan = "April"
        break;
    }
    case 5: {
        bulan = "Mei"
        break;
    }
    case 6: {
        bulan = "Juni"
        break;
    }
    case 7: {
        bulan = "Juli"
        break;
    }
    case 8: {
        bulan = "Agustus"
        break;
    }
    case 9: {
        bulan = "September"
        break;    
    }
    case 10: {
        bulan = "Oktober"
        break;
    }
    case 11: {
        bulan = "November"
        break;
    }
    case 12: {
        bulan = "Desember"
        break;
    }
    default: {
        console.log();
    }
}

function new_date(tanggal) {
    return tanggal + 1;
}

console.log(new_date(13) + " ".concat(bulan + " ", tahun));
